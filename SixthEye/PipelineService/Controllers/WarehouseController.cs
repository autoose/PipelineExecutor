using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SixthEye.Infrastructure;

namespace PipelineService.Controllers;

[Route("api/[controller]")]
[ApiController]
public class WarehouseController : Controller
{ 
    
    [HttpGet]
    public async Task<IActionResult> List()
    {
        var dataset = await Services.RecordStorage.List(); 
        return new JsonResult(dataset);
    }
    
    [HttpGet("{partition}")]
    public async Task<IActionResult> Get(string partition)
    {
        var dataset = await Services.RecordStorage.Get(partition); 
        return new JsonResult(dataset);
    }
}