﻿
using System.Collections.Generic;

namespace SixthEye.Infrastructure.Application
{
    public class SubscribeRequestBody : IMessageBody
    { 
        public string module { get; set; }  
        public IDictionary<string, string> parameters { get; set; }
    }
}