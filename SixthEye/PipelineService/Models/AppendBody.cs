namespace SixthEye.Infrastructure.Application
{
    public class AppendBody : IMessageBody
    {
        public string wasm { get; set; }
    }
}