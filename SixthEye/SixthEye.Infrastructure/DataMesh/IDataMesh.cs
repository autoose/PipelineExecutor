using System;

namespace SixthEye.Infrastructure.DataMesh
{
    public interface IDataMesh
    {
        public void Send(ITopic topic, object message);
        public void ReceiveListener<T>(ITopic topic, Action<T> func);
    }
}