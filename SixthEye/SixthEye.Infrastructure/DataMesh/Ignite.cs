using System;
using System.Collections.Generic;
using System.Linq;
using Apache.Ignite.Core;
using Apache.Ignite.Core.Cache;
using Apache.Ignite.Core.Datastream;
using Apache.Ignite.Core.Discovery.Tcp;
using Apache.Ignite.Core.Discovery.Tcp.Multicast;
using Apache.Ignite.Core.Discovery.Tcp.Static;
using Apache.Ignite.Core.Messaging;
using Google.Protobuf.WellKnownTypes;

namespace SixthEye.Infrastructure.DataMesh
{
    public class Ignite : IDataMesh
    {
        private IIgnite _ignite;
        private IMessaging _messaging;

        private class MessageListener<T> : IMessageListener<T>
        {
            public Action<T> _function;
              
            public bool Invoke(Guid nodeId, T message)
            {
                _function.Invoke(message);
                return true;
            }
        }
        
        public Ignite()
        {
            var cfg = new IgniteConfiguration
            {
                DiscoverySpi = new TcpDiscoverySpi
                {
                    IpFinder = new TcpDiscoveryMulticastIpFinder()
                    {
                        Endpoints = new [] {"127.0.0.1"}
                    }
                }
            };
            _ignite = Ignition.Start(cfg); 
            _messaging = _ignite.GetMessaging(); 
        }

        public string GetTopicString(ITopic topic) =>
            (topic as IgniteTopic)?.TopicString ?? throw new Exception("Invalid topic");

        public void Send<T>(ITopic topic, T value) { 
            //_messaging.Send(message, GetTopicString(topic));
            
            using var streamer = _ignite.GetDataStreamer<Guid, T>(GetTopicString(topic));
            streamer.AddData(new Guid(), value); 
        }

        public void Send(ITopic topic, object message)
        {
            throw new NotImplementedException();
        }

        public void ReceiveListener<T>(ITopic topic, Action<T> func) {
            //var listener = new MessageListener<T> {_function = func};
            //_messaging.RemoteListen(listener, GetTopicString(topic));
             
            using var streamer = _ignite.GetDataStreamer<Guid, T>(GetTopicString(topic));
            streamer.Receiver = new Receiver<T>(func); 
        }

        private class Receiver<T> : IStreamReceiver<Guid, T>
        {
            private Action<T> _action;
            public Receiver(Action<T> action) {
                _action = action;
            }
            public void Receive(ICache<Guid, T> cache, ICollection<ICacheEntry<Guid, T>> entries)
            {
                foreach(var kvp in entries)
                    _action.Invoke(kvp.Value); 
            }
        } 
    }
}