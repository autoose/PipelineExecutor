﻿using System;
using System.Data; 
using System.Threading.Tasks;
using Dapper; 
using Microsoft.Extensions.Logging;
using Sentry;

namespace SixthEye.Infrastructure.Queries
{ 
    public class Subscription : Core.PersistentObjects.Subscription
    {
        private static string UpdateSQL = "UPDATE dbo.Articles SET term = @term, last_updated = @last_updated WHERE id = @id";
        private static string InsertSQL = "INSERT INTO dbo.Subscriptions (term, last_updated) VALUES (@term, @last_updated)";
        private static string GetSQL = "SELECT * FROM dbo.Subscriptions WHERE id = @id";
         
        public bool Exists { get; set; } = true;
        
        public Subscription(IDbConnection connection, int id) {
            try{
                var sub = connection.QueryFirstOrDefault<Core.PersistentObjects.Subscription>(GetSQL, new { id });

                if (sub == null) {
                    Exists = false;
                    return; 
                } 

                this.id = sub.id;
                this.term = sub.term;
                this.last_updated = sub.last_updated; 
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
            }
        } 

        /// <summary>
        /// Inserts the article into the database
        /// </summary>
        /// <param name="connection"></param> 
        public void Commit(IDbConnection connection) 
        {
            try{
                // Computed variables
                this.last_updated = DateTime.UtcNow;
                
                connection.Execute(Exists ? UpdateSQL : InsertSQL, this); 
            }
            catch (Exception ex) {
                Services.Log<Subscription>().LogError(ex.Message, ex);
            } 
        }
        
        
        /// <summary>
        /// Inserts the article into the database
        /// </summary>
        /// <param name="connection"></param> 
        public async Task CommitAsync(IDbConnection connection) 
        {
            try{
                // Computed variables
                this.last_updated = DateTime.UtcNow;
                
                await connection.ExecuteAsync(Exists ? UpdateSQL : InsertSQL, this);  
            }
            catch (Exception ex) {
                Services.Log<Subscription>().LogError(ex.Message, ex);
            }
        }
    } 
}