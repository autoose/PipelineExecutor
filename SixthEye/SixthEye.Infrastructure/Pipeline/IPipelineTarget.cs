﻿using System;

namespace SixthEye.Infrastructure.Pipeline.Target
{
    public interface IPipelineTarget<T>: ISelfReporting, IDisposable
    {
        void Send(T value); 
    }
}