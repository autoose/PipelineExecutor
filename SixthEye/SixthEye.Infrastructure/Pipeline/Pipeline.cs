﻿using System; 
using System.Collections.Generic;
using System.Diagnostics;
using System.Reactive.Concurrency;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Microsoft.Extensions.Logging;
using Sentry;
using SixthEye.Infrastructure.Application;

namespace SixthEye.Infrastructure.Pipeline
{
    public class Pipeline<sT> 
    {
        public string Identifier { get; set; }
        
        public readonly CancellationTokenSource CancellationTokenSource = new();
        protected readonly StageReporting _reporting = new(); 

        protected BufferBlock<sT> _first_buffer;
        protected IDataflowBlock _last_node; 

        protected readonly DataflowLinkOptions Linker = new() {
            PropagateCompletion = true
        };
        protected readonly List<IDataflowBlock> BufferBlocks = new();
 
        private ExecutionDataflowBlockOptions DefaultTransformSettings => new () {
            BoundedCapacity = 512, 
            MaxDegreeOfParallelism = 512,
            CancellationToken = CancellationTokenSource.Token, 
            // TODO It may be a good idea for a thread pool with better IO scaling
            //TaskScheduler = new PipelineTaskScheduler() 
        };

        protected DataflowBlockOptions DefaultCapacitySettings => new() {
            BoundedCapacity = 512, 
            CancellationToken = CancellationTokenSource.Token
        };
        
        protected ISourceBlock<sT> LastNode
        {
            get {
                if (_last_node is not ISourceBlock<sT> lastNode)
                    throw new PipelineBuildException("Type miss match with previous step");
                return lastNode;
            }
        }
        
        protected void Install<TOutput>(IPropagatorBlock<sT, TOutput> block) {
            var buffer = new BufferBlock<TOutput>(DefaultCapacitySettings);
            BufferBlocks.Add(buffer);
            BufferBlocks.Add(block);
             
            LastNode.LinkTo(block, Linker);
            block.LinkTo(buffer, Linker);

            Console.WriteLine($"Install {_last_node} -> {block}");
            Console.WriteLine($"Install {block} -> {buffer}");

            _last_node = buffer; 
        }
        
        protected void InstallAsync<TOutput>(IPropagatorBlock<sT, Task<TOutput>> block)
        {
            var buffer = new BufferBlock<Task<TOutput>>(DefaultCapacitySettings);
            BufferBlocks.Add(buffer);
            BufferBlocks.Add(block);

            LastNode.LinkTo(block, Linker); 
            block.LinkTo(buffer, Linker);

            Console.WriteLine($"Install {_last_node} -> {block} -> {buffer}");
            
            _last_node = buffer; 
        }

        protected Func<T, TOut> Wrap<T, TOut>(Func<T, TOut> lambda, string identifier) => (value) => { 
         
            if (CancellationTokenSource.IsCancellationRequested) {
                // TODO State preservation on pipeline SEE MORE
                // We need to return the state or let the pipeline finish, maybe this function could be used as a FORCE STOP
                // Mayber we should use a ACK mechanism on the state to flag IS WORKING and COMPLETE, ISWORKING to prevent others from picking up and 
                // COMPLETE to be able to preserve state that's dropped from these procedures or normal FATALS
                return default;
            }
            
            var watch = Stopwatch.StartNew(); 
            var errors = 0;
            Interlocked.Increment(ref _reporting._flight);
            try
            { 
                var result = lambda(value); 
                return result;
            }
            catch (Exception ex)
            {
                Services.Log.LogError("Pipeline Wrapped Exception", ex);
                SentrySdk.CaptureException(ex); 
                errors++;
            }
            finally
            {
                watch.Stop();
                Interlocked.Decrement(ref _reporting._flight);
                Services.Metrics.Report("Pipeline", identifier ?? "unidentified",
                    new {Duration = watch.Elapsed.TotalSeconds, Volume = 1, Error = errors});
            }

            
            return default;
        };
        
        protected Func<T, Task<TOut>> WrapAsync<T, TOut>(Func<T, Task<TOut>> lambda, string identifier) => (value) =>
        {
            var watch = Stopwatch.StartNew();
            var errors = 0;
            Interlocked.Increment(ref _reporting._flight);
            try
            { 
                var result = lambda(value); 
                return result;
            }
            catch (Exception ex)
            {
                Services.Log.LogError("Pipeline Wrapped Exception", ex);
                SentrySdk.CaptureException(ex);
                errors++;
            }
            finally
            {
                watch.Stop();
                Interlocked.Decrement(ref _reporting._flight);
                Services.Metrics.Report("Pipeline", identifier ?? "unidentified",
                    new {Duration = watch.Elapsed.TotalSeconds, Volume = 1, Error = errors});
            }

            return default;
        };

        protected Action<T> Wrap<T>(Action<T> lambda, string identifier) => (value) =>
        {
            var watch = Stopwatch.StartNew();
            var errors = 0;
            Interlocked.Increment(ref _reporting._flight);
            try {
                lambda(value);
            }
            catch (Exception ex) {
                errors++;
                Interlocked.Decrement(ref _reporting._flight);
                Services.Log.LogError("Pipeline Wrapped Exception", ex);
                SentrySdk.CaptureException(ex);
            }
            finally
            {
                watch.Stop();
                Services.Metrics.Report("Pipeline", identifier ?? "unidentified",
                    new {Duration = watch.Elapsed.TotalSeconds, Volume = 1, Error = errors});
            }
        };

        protected IPropagatorBlock<sT, TOut> TransformWrap<TOut>(Func<sT, TOut> func, string identifier, ExecutionDataflowBlockOptions settings = null) =>
            new TransformBlock<sT, TOut>(Wrap(func, identifier), settings ?? DefaultTransformSettings);
        
        protected IPropagatorBlock<sT, Task<TOut>> TransformWrapAsync<TOut>(Func<sT, Task<TOut>> func, string identifier, ExecutionDataflowBlockOptions settings = null) =>
            new TransformBlock<sT, Task<TOut>>(WrapAsync(func, identifier), settings ?? DefaultTransformSettings);

        protected IPropagatorBlock<T, TOut> TransformWrap<T, TOut>(Func<T, TOut[]> func, string identifier, ExecutionDataflowBlockOptions settings = null) =>
            new TransformManyBlock<T, TOut>(Wrap(func, identifier), settings ?? DefaultTransformSettings);
 
    }
}