using System.Linq;
using SixthEye.Infrastructure.Application;

namespace SixthEye.Infrastructure.Pipeline.Target;

public class RecordTarget : IPipelineTarget<State>
{
    private readonly IRecordStore _store = Services.RecordStorage; 
    private readonly string _partition;
    
    private readonly StageReporting _reporting = new(); 

    public RecordTarget(string partition)
    {
        _partition = partition; 
    }
    public void Send(State value) {
        _store.Put(value, _partition, value.variables.Keys.ToArray());
        _reporting.Processed++;
    }

    public object SelfReport()
    {
        return new { _queue = _partition };
    }

    public StageReporting Counters() => _reporting;

    public void Dispose() { }
}