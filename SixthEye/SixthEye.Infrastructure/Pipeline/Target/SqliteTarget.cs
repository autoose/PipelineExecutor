using System;
using System.Threading;
using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
using Sentry;
using SixthEye.Infrastructure.Application;

namespace SixthEye.Infrastructure.Pipeline.Target;

public class SqliteTarget : IPipelineTarget<State>
{
    private readonly StageReporting _reporting = new(); 

    private readonly SqliteConnection _connection; 
    private readonly string _exchange;
    private readonly string _queue;

    private SqliteParameter _idParam;
    private SqliteParameter _variableParam;
  
    struct ThreadPack {
        public SqliteCommand command;
        public SqliteParameter id;
        public SqliteParameter variables; 
    }

    private readonly ThreadLocal<ThreadPack> _commandCache;

    public SqliteTarget(string exchange, string queue) { 
        _connection = ConnectionFactory.GetConnection(":memory:"); 
        _exchange = exchange;
        _queue = queue;
          
        var tableCommand = _connection.CreateCommand(); 
        tableCommand.CommandText = $"CREATE TABLE IF NOT EXISTS {_queue} (id varchar PRIMARY KEY, variables varchar) WITHOUT ROWID";
        tableCommand.ExecuteNonQuery();
        
        lock(_connection)
            tableCommand.Dispose();

        _commandCache = new ThreadLocal<ThreadPack>(() => {
            var command = _connection.CreateCommand();
            command.CommandText = $"INSERT INTO {_queue} (id, variables) VALUES ($id, $variables)";
            return new ThreadPack() {
                command = command,
                id = command.Parameters.Add("id", SqliteType.Text),
                variables = command.Parameters.Add("variables", SqliteType.Text)
            };
        });

    }
    

    public void Send(State state)
    {
        var sqlPack = _commandCache.Value;
         
        try {
            sqlPack.id.Value = state.Id;
            sqlPack.variables.Value = JsonConvert.SerializeObject(state.variables);  
            sqlPack.command.ExecuteNonQuery();
            _reporting.AppendPeek(state);
            _reporting.Processed++;
        }
        catch (Exception ex) {
            SentrySdk.CaptureException(ex);
        }
    }
    
    public object SelfReport()
    {
        return new { _connection.State };
    }

    public StageReporting Counters()
    {
        return _reporting;
    }

    public void Dispose()
    {
        lock(_connection)
        {
            foreach (var pack in _commandCache.Values) 
                pack.command?.Dispose(); 
        }
        _commandCache?.Dispose();
    }
}