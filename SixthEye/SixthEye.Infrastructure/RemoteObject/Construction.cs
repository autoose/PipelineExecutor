﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata.Ecma335;
using System.Security.Cryptography;
using System.Text;
using AngleSharp.Html.Forms;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using SixthEye.Infrastructure.RemoteObject.Attributes;
using Index = System.Index;

namespace SixthEye.Infrastructure.RemoteObject
{
    
    
    internal static class Extension{
        public static T AggregateCheck<T>(this IEnumerable<T> query, Func<T, T, T> formatter) where T : class
        {
            if (query.Any())
                return query.Aggregate(formatter);
            return default;
        }
    }
    /// <summary>
    /// This class will construct SQL queries to query the database for an object, mapping to the object type T
    /// 
    /// </summary>
    internal class Construction
    { 
        internal class TypeInfo
        {
            public List<PropertyInfo> IndexVariables = new List<PropertyInfo>();
            public List<(PropertyInfo prop, ScanOperations operation)> FilterOperations = new List<(PropertyInfo prop, ScanOperations operation)>(); 
            public List<PropertyInfo> Fields = new List<PropertyInfo>();
            public PropertyInfo Key;
            public string TypeName;
        }
        
        private static long ComputeHash(string value)
        {
            if(string.IsNullOrWhiteSpace(value))
                return 0;
            using var sha256 = new SHA256Managed();
            var hex = BitConverter.ToString(sha256.ComputeHash(Encoding.ASCII.GetBytes(value))).Replace("-", "");
            return long.Parse(hex.AsSpan(0, 16), NumberStyles.HexNumber);
        }

        private static string GetIndexVariableNames(TypeInfo type) =>
            type.IndexVariables.OrderBy(x => x.Name).Select(x => $"{x.Name}_index").AggregateCheck((x, y) => $"{x}, {y}");
        
        private static string GetFieldVariableNames(TypeInfo type) =>
            type.Fields
                .Where(x => x != type.Key)
                .OrderBy(x => x.Name).Select(x => $"{x.Name}").AggregateCheck((x, y) => $"{x}, {y}");

        private static string GetScanVariableNames(TypeInfo type) =>
            type.FilterOperations.OrderBy(x => x.prop.Name)
                .Select(x => $"{x.prop.Name}_scan").AggregateCheck((x, y) => $"{x}, {y}");

        private static string GetIndexVariableValues<T>(TypeInfo type, T value) =>
            type.IndexVariables.OrderBy(x => x.Name)
                .Select(x => ComputeHash(x.GetValue(value)?.ToString()).ToString())
                .AggregateCheck((x, y) => $"{x}, {y}");
        
        private static string GetFieldVariableValues<T>(TypeInfo type, T value) =>
            type.Fields
                .Where(x => x != type.Key)
                .OrderBy(x => x.Name)
                .Select(x =>
                {
                    var v = x.GetValue(value);
                    if (v is string)
                        return $"'{v}'";
                    return v?.ToString() ?? "null";
                }) 
                .AggregateCheck((x, y) => $"{x}, {y}");

        private static string GetScanVariableValues<T>(TypeInfo type, T value) =>
            type.FilterOperations.OrderBy(x => x.prop.Name)
                .Select(x => Scan.ExecuteOperation(x.prop.GetValue(value), x.operation).ToString())
                .AggregateCheck((x, y) => $"{x}, {y}");

        private static string GetFieldSetters<T>(TypeInfo type, T value) =>
            type.Fields.OrderBy(x => x.Name).Select(x => $"{x.Name}={x.GetValue(value)}").AggregateCheck((a, b) => $"{a}, {b}");

        private static string GetFilterSetters<T>(TypeInfo type, T value) =>
            type.FilterOperations
                .OrderBy(x => x.prop.Name)
                .Select(x => $"{x.prop.Name}_filter={Scan.ExecuteOperation(value, x.operation)}")
                .AggregateCheck((a, b) => $"{a} {b}");
        
        private static string GetIndexSetters<T>(TypeInfo type, T value) =>
            type.IndexVariables
                .OrderBy(x => x.Name)
                .Select(x => $"{x.Name}_index={ComputeHash($"{x.GetValue(value)}")}") 
                .AggregateCheck((a, b) => $"{a} {b}");
        
        public static TypeInfo ExtractTypeInfo<T>()
        {
            var type = typeof(T); 
            var result = new TypeInfo();
            result.TypeName = type.FullName ?? $"Unknown+{type.Name}";
            result.TypeName = result.TypeName.Replace('+', '_').Replace('.', '_');

            foreach (var prop in type.GetProperties())
            {
                result.Fields.Add(prop);
                
                foreach(var attribute in prop.GetCustomAttributes(false)){
                    switch (attribute)
                    {
                        case Attributes.Index _:
                            result.IndexVariables.Add(prop);
                            break;
                        case Attributes.Scan _:
                            result.FilterOperations.Add((prop, (attribute as Scan).Operation));
                            break;
                        case Attributes.Key _:
                            result.Key = prop;
                            break;
                    } 
                }
            }

            return result;
        }

        
        // Construct a SQL query to get the info type, index values provided should be defined as an index in type definition.
        public static T Build<T>(TypeInfo info, object indexValues, IDbConnection connection) where T : class
        {
            var where = info.IndexVariables
                .Select(x => $"{x.Name} = @{x.Name}")
                .AggregateCheck((x, y) => $"{x} AND {y}");

            var query = $"SELECT TOP(1) * FROM {info.TypeName} WHERE {where}";
            
            return connection.Query(query, indexValues).FirstOrDefault() as T;
        }
        
        public static bool Commit<T>(TypeInfo info, T value, IDbConnection connection) where T : class
        { 
            // Construct the values that we use for index 
            // All ScanAttributes require a computed boolean for an operation, the result is attached to an index.
            // All IndexAttributes require a computed hash for fast access. (SHA2)

            if (!TableExists<T>(info.TypeName, connection))
            {
                var table = CreateTable<T>(info, connection);
                connection.Execute(table);
            }

            var names = new[] {GetFieldVariableNames(info), GetIndexVariableNames(info), GetScanVariableNames(info)} 
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .AggregateCheck((a, b) => $"{a}, {b}");

            var values = new [] {GetFieldVariableValues(info, value), GetIndexVariableValues(info, value), GetScanVariableValues(info, value)}
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .AggregateCheck((a, b) => $"{a}, {b}");
                  
            var query = $"INSERT INTO {info.TypeName} ({names}) VALUES ({values})";
               
            return connection.Execute(query) == 1; 
        }

        public static bool Update<T>(TypeInfo info, T value, IDbConnection connection) where T : class
        {
            var variables = GetFilterSetters(info, value) + GetIndexSetters(info, value) + GetFieldSetters(info, value);
            
            var query = $"UPDATE {info.TypeName} SET {variables} WHERE {info.Key.Name}={info.Key.GetValue(value)}";
             
            return connection.Execute(query) == 1;  
        }

        private static HashSet<string> Tables = new HashSet<string>();
        private static bool TableExists<T>(string TypeName, IDbConnection connection)
        {
            if (Tables.Contains(TypeName)) 
                return true;

            var exists = connection.ExecuteScalar<bool>($"SELECT TOP(1) 1 FROM SYS.TABLES WHERE NAME = '{TypeName}'");
            
            if(exists)
                Tables.Add(TypeName);

            return exists;
        }


        
        private static string CreateTable<T>(TypeInfo info, IDbConnection connection)
        {
            var fields = info.Fields.SelectMany(ToSqlDataType).AggregateCheck((x, y) => $"{x}, {y}");

            //var scan = info.FilterOperations.Select(x => x.prop.Name).AggregateCheck((x, y) => $"{x}, {y}");
            //scan = string.IsNullOrWhiteSpace(scan) ? info.Key.Name : $"{info.Key.Name}, {scan}";
            
            var table = $"CREATE TABLE {info.TypeName} ({fields})";

            var index = info.IndexVariables
                .Select(idx => $"CREATE INDEX {info.TypeName}_index ON {info.TypeName} ({idx.Name}_index) WITH (FILLFACTOR = 80);\r\n")
                .AggregateCheck((x, y) => $"{x}, {y}");

            return table + ";\r\n" + index; 
        }


        private static Dictionary<Type, Func<PropertyInfo, string>> _typeConversion
            = new Dictionary<Type, Func<PropertyInfo, string>>()
            {
                {typeof(string), info => $"nvarchar({info.GetCustomAttribute<Length>()?.value ?? 4000})"},
                {typeof(DateTime), info => $"datetime2"},
                {typeof(decimal), info => $"decimal"},
                {typeof(float), info => $"float"},
                {typeof(bool), info => $"bit"}, 
                {typeof(int), info => $"int"}, 
                {typeof(short), info => $"tinyint"}, 
                {typeof(long), info => $"bigint"}
            };

        private static IEnumerable<string> ToSqlDataType(PropertyInfo type)
        {
            if (_typeConversion.TryGetValue(type.PropertyType, out var converter))
            {
                var key = type.GetCustomAttribute<Key>();
                var index = type.GetCustomAttribute<Attributes.Index>();
                var scan = type.GetCustomAttribute<Scan>();
                
                if (key != null) 
                    return new [] {$"{type.Name} {converter(type)} IDENTITY PRIMARY KEY CLUSTERED WITH (OPTIMIZE_FOR_SEQUENTIAL_KEY = ON) "}; 
                if (index != null) 
                    return new[]
                    {
                        $"{type.Name} {converter(type)}",
                        $"{type.Name}_index bigint"
                    }; 

                if (scan != null) 
                    return new[]
                    {
                        $"{type.Name} {converter(type)}",
                        $"{type.Name}_scan bit"
                    };
 
                return new [] {$"{type.Name} {converter(type)}"}; 
            }
            throw new Exception($"Unknown datatype {type.Name} {type.PropertyType.Name}");
        }
    }
}