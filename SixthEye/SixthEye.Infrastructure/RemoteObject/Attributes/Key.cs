﻿using System;

namespace SixthEye.Infrastructure.RemoteObject.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class Key : Attribute
    {
        
    }
}