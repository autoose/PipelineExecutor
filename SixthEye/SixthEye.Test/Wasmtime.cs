﻿using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SixthEye.Infrastructure.Application;
using SixthEye.Infrastructure;
using SixthEye.Infrastructure.Executor;

namespace SixthEye.Test;

[TestClass]
public class Wasmtime
{
    private IExecutor _executor;
    private readonly string FilePath = "/Users/joshuawaring/Documents/wasmodule/wapm_packages/rustpython/rustpython@0.1.3/target/wasm32-wasi/release/rustpython.wasm";
    
    
    [TestMethod]
    public void Build()
    { 
        _executor = new WasmtimeExecutor(File.ReadAllBytes(FilePath), Path.GetFileNameWithoutExtension(FilePath));
        _executor.Init("start", null);
    }

   // [TestMethod]
    public async Task Run()
    {
        _executor = new WasmtimeExecutor(File.ReadAllBytes(FilePath), Path.GetFileNameWithoutExtension(FilePath));
        _executor.Init("start", null);
        Assert.AreNotEqual(0, (await _executor.Run(new State())).Length);
    }

  //  [TestMethod]
    public void ImportState()
    {
        _executor = new WasmtimeExecutor(File.ReadAllBytes("/home/joshua/Git/assemblyscript/build/assemblyscript.release.wasm"), "assemblyscript");
        _executor.Init("start", null);
        //_executor.Run(state);
        
    }
}