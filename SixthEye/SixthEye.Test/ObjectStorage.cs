﻿ 
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SixthEye.Infrastructure.Application; 
using SixthEye.Infrastructure.ObjectStorage; 

namespace SixthEye.Test;

[TestClass]
public class ObjectStorage
{ 
    private IObjectStorage _objectStorage;

    [TestInitialize]
    public void setup() {
        _objectStorage = new MinIOStorage("unit-tests");
    }
    
    [TestMethod]
    public async Task Put() {
        using var stream = new MemoryStream();
        stream.Write(Encoding.ASCII.GetBytes("This is a string"));
        stream.Seek(0, SeekOrigin.Begin);
        await _objectStorage.Put(stream, nameof(Put), nameof(ObjectStorage));
    }

    [TestMethod]
    public async Task List() {
        await _objectStorage.List(nameof(ObjectStorage));
    }


    /// <summary>
    /// Test depends on a PASSING put test
    /// </summary>
    [TestMethod]
    public async Task Get() {
        using var stream = new MemoryStream();
        stream.Write(Encoding.ASCII.GetBytes("This is a string"));
        stream.Seek(0, SeekOrigin.Begin);
        await _objectStorage.Put(stream, nameof(Get), nameof(ObjectStorage));

        var responseStream = await _objectStorage.Get(nameof(Get), nameof(ObjectStorage));
        responseStream.Seek(0, SeekOrigin.Begin); 
        Assert.AreEqual('T', responseStream.ReadByte());
    }

    [TestMethod]
    public async Task Delete()
    {
        await _objectStorage.Delete(nameof(Put), nameof(ObjectStorage)); 
        await _objectStorage.Delete(nameof(Get), nameof(ObjectStorage));
    }
     
}

