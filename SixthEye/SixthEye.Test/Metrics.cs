using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SixthEye.Infrastructure;
using SixthEye.Infrastructure.Metrics;

namespace SixthEye.Test;

[TestClass]
public class Metrics : IDisposable
{
    private readonly IMetrics _metrics = new InfluxMetrics($"http://{Services.ServerHost}:8086", "development", "CyNH96QbCjpfGpB58ibReEBl9QIVyy3osYgT7XpcN2PSD1qxsjWTG-wsBekUDwvKsNGoZi-cTG4PHB5iW4iH-Q==");
    
    [TestMethod]
    public void Report() {
        _metrics.Report(nameof(Metrics), nameof(Report), new {
                str = "",
                i = "",
                f = Math.PI,
                t = TimeSpan.FromMilliseconds(Math.PI),
                tl = TimeSpan.FromSeconds(1.57)
            });
    }

   // [TestMethod]
    public async Task Get() {
        await _metrics.Get("ce0f7ec6-f8c8-4f59-a5a5-c4c99e674d03");
    }

    public void Dispose() {
        _metrics?.Dispose();
    }
}