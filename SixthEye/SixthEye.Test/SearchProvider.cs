using System.Linq;  
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SixthEye.Core.PersistentObjects;
using SixthEye.Infrastructure; 

namespace SixthEye.Test
{ 
    [TestClass]
    public class SearchTest
    {
        [TestMethod]
        public async void Search()
        {
            var articles = await WebSearch.SearchAsync("AMD");

            Assert.AreNotSame(0,articles.Count(), "No search results returned");
        } 
    }
}
